package com.example.langtonsant;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.example.langtonsant.AntActivity.AntState;
import com.example.langtonsant.AntActivity.AntTurn;

import java.util.ArrayList;
import java.util.Arrays;

public class StateTransitionTableActivity extends AppCompatActivity {

    static int[][] TURMITE_STATE_TRANSITION_TABLE;
    static TurmiteType TURMITE_TYPE;

    static {
        pickLoopy();
    }

    private static void pickFibonacci() {
        TURMITE_TYPE = TurmiteType.FIBONACCI;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickFilledTriangle() {
        TURMITE_TYPE = TurmiteType.FILLED_TRIANGLE;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ONE.ordinal()}};
    }

    private static void pickBoxSpiral() {
        TURMITE_TYPE = TurmiteType.BOX_SPIRAL;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ONE.ordinal()}};
    }

    private static void pickStripedSpiral() {
        TURMITE_TYPE = TurmiteType.STRIPED_SPIRAL;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ZERO.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickSteppedPyramid() {
        TURMITE_TYPE = TurmiteType.STEPPED_PYRAMID;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ZERO.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickContouredIsland() {
        TURMITE_TYPE = TurmiteType.CONTOURED_ISLAND;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickWovenPlacement() {
        TURMITE_TYPE = TurmiteType.WOVEN_PLACEMENT;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ONE.ordinal()}};
    }

    private static void pickSnowflakeIsh() {
        TURMITE_TYPE = TurmiteType.SNOWFLAKE_ISH;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickSlowCityBuilder() {
        TURMITE_TYPE = TurmiteType.SLOW_CITY_BUILDER;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ONE.ordinal()}};
    }

    private static void pickComputerArt() {
        TURMITE_TYPE = TurmiteType.COMPUTER_ART;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ONE.ordinal()}};
    }

    private static void pickBalloonBursting() {
        TURMITE_TYPE = TurmiteType.BALLOON_BURSTING;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickHorizontalHighway() {
        TURMITE_TYPE = TurmiteType.HORIZONTAL_HIGHWAY;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ZERO.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickHighway1() {
        TURMITE_TYPE = TurmiteType.HIGHWAY1;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ZERO.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickHighway2() {
        TURMITE_TYPE = TurmiteType.HIGHWAY2;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickFilledSpiral() {
        TURMITE_TYPE = TurmiteType.FILLED_SPIRAL;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ZERO.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickGlaciers() {
        TURMITE_TYPE = TurmiteType.GLACIERS;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ZERO.ordinal()}, {AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickFizzySpill() {
        TURMITE_TYPE = TurmiteType.FIZZY_SPILL;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ZERO.ordinal()}, {AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickNestedCabinets() {
        TURMITE_TYPE = TurmiteType.NESTED_CABINETS;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ONE.ordinal()}};
    }

    private static void pickCross() {
        TURMITE_TYPE = TurmiteType.CROSS;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ONE.ordinal()}};
    }

    private static void pickSaw() {
        TURMITE_TYPE = TurmiteType.SAW;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ZERO.ordinal()}, {AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickCurvesInBlocks() {
        TURMITE_TYPE = TurmiteType.CURVES_IN_BLOCKS;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickTextured() {
        TURMITE_TYPE = TurmiteType.TEXTURED;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ZERO.ordinal()}, {AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickDiamond() {
        TURMITE_TYPE = TurmiteType.DIAMOND;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickCoiledRope() {
        TURMITE_TYPE = TurmiteType.COILED_ROPE;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ZERO.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickGrowth1() {
        TURMITE_TYPE = TurmiteType.GROWTH1;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ONE.ordinal()}};
    }

    private static void pickSquareSpiral() {
        TURMITE_TYPE = TurmiteType.SQUARE_SPIRAL;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ONE.ordinal()}};
    }

    private static void pickLoopy() {
        TURMITE_TYPE = TurmiteType.LOOPY;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ONE.ordinal()}};
    }

    private static void pickSquareAnt() {
        TURMITE_TYPE = TurmiteType.SQUARE_ANT;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickGrowth2() {
        TURMITE_TYPE = TurmiteType.GROWTH2;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ZERO.ordinal()}, {AntActivity.COLOR_WHITE, AntTurn.L.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickDistractedSpiral() {
        TURMITE_TYPE = TurmiteType.DISTRACTED_SPIRAL;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ZERO.ordinal()}, {AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal()}};
    }

    private static void pickCauliflowerStalk() {
        TURMITE_TYPE = TurmiteType.CAULIFLOWER_STALK;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ONE.ordinal()}};
    }

    private static void pickWormTrails() {
        TURMITE_TYPE = TurmiteType.WORM_TRAILS;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickTwoWayHighway() {
        TURMITE_TYPE = TurmiteType.TWO_WAY_HIGHWAY;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ZERO.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ONE.ordinal()}};
    }

    private static void pickMouldBloom() {
        TURMITE_TYPE = TurmiteType.MOULD_BLOOM;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ZERO.ordinal()}, {AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_WHITE, AntTurn.N.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickHighway3() {
        TURMITE_TYPE = TurmiteType.HIGHWAY3;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ZERO.ordinal()}, {AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ZERO.ordinal(), AntActivity.COLOR_BLACK, AntTurn.N.ordinal(), AntState.ONE.ordinal()}};
    }

    private static void pickHighway4() {
        TURMITE_TYPE = TurmiteType.HIGHWAY4;
        TURMITE_STATE_TRANSITION_TABLE = new int[][]{{AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_BLACK, AntTurn.R.ordinal(), AntState.ONE.ordinal()}, {AntActivity.COLOR_BLACK, AntTurn.L.ordinal(), AntState.ONE.ordinal(), AntActivity.COLOR_WHITE, AntTurn.R.ordinal(), AntState.ZERO.ordinal()}};
    }

    private static void pickNone() {
        TURMITE_TYPE = TurmiteType.NONE;
    }

    private final ArrayList<String> turmitesItems = new ArrayList<>(Arrays.asList("None",
            "fibonacci", "filled-triangle", "box-spiral", "striped-spiral", "stepped-pyramid", "contoured-island",
            "woven-placement", "snowflake-ish", "slow-city-builder", "computer-art", "balloon-bursting", "horizontal-highway",
            "highway1", "highway2", "filled-spiral", "glaciers", "fizzy-spill", "nested-cabinets",
            "cross", "saw", "curves-in-blocks", "textured", "diamond", "coiled-rope",
            "growth1", "square-spiral", "loopy", "square-ant", "growth2", "distracted-spiral",
            "cauliflower-stalk", "worm-trails", "two-way-highway", "mould-bloom", "highway3", "highway4"));
    private Spinner turmitesDropdown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_transition_table);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("State transition table"); //Turmite transition table

        updateTableButtons();

        turmitesDropdown = findViewById(R.id.turmitesDropdown);
        turmitesDropdown.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, turmitesItems));
        turmitesDropdown.setSelection(TURMITE_TYPE.ordinal());
        turmitesDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedOption = parent.getItemAtPosition(position).toString();

                switch (selectedOption) {
                    case "fibonacci":
                        pickFibonacci();
                        break;
                    case "filled-triangle":
                        pickFilledTriangle();
                        break;
                    case "box-spiral":
                        pickBoxSpiral();
                        break;
                    case "striped-spiral":
                        pickStripedSpiral();
                        break;
                    case "stepped-pyramid":
                        pickSteppedPyramid();
                        break;
                    case "contoured-island":
                        pickContouredIsland();
                        break;
                    case "woven-placement":
                        pickWovenPlacement();
                        break;
                    case "snowflake-ish":
                        pickSnowflakeIsh();
                        break;
                    case "slow-city-builder":
                        pickSlowCityBuilder();
                        break;
                    case "computer-art":
                        pickComputerArt();
                        break;
                    case "balloon-bursting":
                        pickBalloonBursting();
                        break;
                    case "horizontal-highway":
                        pickHorizontalHighway();
                        break;
                    case "highway1":
                        pickHighway1();
                        break;
                    case "highway2":
                        pickHighway2();
                        break;
                    case "filled-spiral":
                        pickFilledSpiral();
                        break;
                    case "glaciers":
                        pickGlaciers();
                        break;
                    case "fizzy-spill":
                        pickFizzySpill();
                        break;
                    case "nested-cabinets":
                        pickNestedCabinets();
                        break;
                    case "cross":
                        pickCross();
                        break;
                    case "saw":
                        pickSaw();
                        break;
                    case "curves-in-blocks":
                        pickCurvesInBlocks();
                        break;
                    case "textured":
                        pickTextured();
                        break;
                    case "diamond":
                        pickDiamond();
                        break;
                    case "coiled-rope":
                        pickCoiledRope();
                        break;
                    case "growth1":
                        pickGrowth1();
                        break;
                    case "square-spiral":
                        pickSquareSpiral();
                        break;
                    case "loopy":
                        pickLoopy();
                        break;
                    case "square-ant":
                        pickSquareAnt();
                        break;
                    case "growth2":
                        pickGrowth2();
                        break;
                    case "distracted-spiral":
                        pickDistractedSpiral();
                        break;
                    case "cauliflower-stalk":
                        pickCauliflowerStalk();
                        break;
                    case "worm-trails":
                        pickWormTrails();
                        break;
                    case "two-way-highway":
                        pickTwoWayHighway();
                        break;
                    case "mould-bloom":
                        pickMouldBloom();
                        break;
                    case "highway3":
                        pickHighway3();
                        break;
                    case "highway4":
                        pickHighway4();
                        break;
                }

                updateTableButtons();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    private void updateTableButtons() {
        for (int i = 0; i < TURMITE_STATE_TRANSITION_TABLE.length; i++) {
            for (int j = 0; j < TURMITE_STATE_TRANSITION_TABLE[i].length; j++) {
                Button button = findViewById(getResources().getIdentifier("tableButton" + i + "" + j, "id", getPackageName()));
                int value = TURMITE_STATE_TRANSITION_TABLE[i][j];
                switch (j % 3) {
                    case 0:
                        button.setText(" "); //(value == AntActivity.COLOR_BLACK ? "B" : "W")
                        button.setBackgroundColor(value);
                        break;
                    case 1:
                        button.setText(AntTurn.values()[value].name());
                        break;
                    case 2:
                        button.setText(String.valueOf(value));
                        break;
                }
            }
        }
    }

    public void writeColor(View view) {
        String id = getResources().getResourceEntryName(view.getId()).replaceFirst("tableButton", "");
        int i = Integer.parseInt(id.charAt(0) + "");
        int j = Integer.parseInt(id.charAt(1) + "");
        int color = TURMITE_STATE_TRANSITION_TABLE[i][j];

        if (color == AntActivity.COLOR_BLACK) {
            //((Button) view).setText("W");
            ((Button) view).setBackgroundColor(AntActivity.COLOR_WHITE);
            TURMITE_STATE_TRANSITION_TABLE[i][j] = AntActivity.COLOR_WHITE;
        } else if (color == AntActivity.COLOR_WHITE) {
            //((Button) view).setText("B");
            ((Button) view).setBackgroundColor(AntActivity.COLOR_BLACK);
            TURMITE_STATE_TRANSITION_TABLE[i][j] = AntActivity.COLOR_BLACK;
        }

        pickNone();
        turmitesDropdown.setSelection(0);
    }

    public void turn(View view) {
        String id = getResources().getResourceEntryName(view.getId()).replaceFirst("tableButton", "");
        int i = Integer.parseInt(id.charAt(0) + "");
        int j = Integer.parseInt(id.charAt(1) + "");
        int turn = TURMITE_STATE_TRANSITION_TABLE[i][j];
        int newTurn = (turn + 1) % 4;

        ((Button) view).setText(AntTurn.values()[newTurn].name());
        TURMITE_STATE_TRANSITION_TABLE[i][j] = newTurn;

        pickNone();
        turmitesDropdown.setSelection(0);
    }

    public void nextState(View view) {
        String id = getResources().getResourceEntryName(view.getId()).replaceFirst("tableButton", "");
        int i = Integer.parseInt(id.charAt(0) + "");
        int j = Integer.parseInt(id.charAt(1) + "");
        int state = TURMITE_STATE_TRANSITION_TABLE[i][j];

        if (state == 0) {
            ((Button) view).setText("1");
            TURMITE_STATE_TRANSITION_TABLE[i][j] = 1;
        } else if (state == 1) {
            ((Button) view).setText("0");
            TURMITE_STATE_TRANSITION_TABLE[i][j] = 0;
        }

        pickNone();
        turmitesDropdown.setSelection(0);
    }

    public void goToPlaying(View view) {
        startActivity(new Intent(this, TurmiteActivity.class));
        //Toast.makeText(this, "Starting", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.extended_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        } else if (id == R.id.menuHelp) {
            Intent intent = new Intent(this, HelpActivity.class);
            startActivity(intent);
        }
        return true;
    }

    enum TurmiteType {
        NONE,
        FIBONACCI, FILLED_TRIANGLE, BOX_SPIRAL, STRIPED_SPIRAL, STEPPED_PYRAMID, CONTOURED_ISLAND,
        WOVEN_PLACEMENT, SNOWFLAKE_ISH, SLOW_CITY_BUILDER, COMPUTER_ART, BALLOON_BURSTING, HORIZONTAL_HIGHWAY,
        HIGHWAY1, HIGHWAY2, FILLED_SPIRAL, GLACIERS, FIZZY_SPILL, NESTED_CABINETS,
        CROSS, SAW, CURVES_IN_BLOCKS, TEXTURED, DIAMOND, COILED_ROPE,
        GROWTH1, SQUARE_SPIRAL, LOOPY, SQUARE_ANT, GROWTH2, DISTRACTED_SPIRAL,
        CAULIFLOWER_STALK, WORM_TRAILS, TWO_WAY_HIGHWAY, MOULD_BLOOM, HIGHWAY3, HIGHWAY4
    }

}