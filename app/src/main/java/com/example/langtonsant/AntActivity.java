package com.example.langtonsant;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import com.otaliastudios.zoom.ZoomLayout;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public abstract class AntActivity extends AppCompatActivity {

    static final int COLOR_WHITE = Color.rgb(255, 255, 255);
    static final int COLOR_BLACK = Color.rgb(50, 50, 50);

    private int numberOfRows, numberOfColumns;
    private GridLayout gridLayout;
    protected ArrayList<ArrayList<Boolean>> lattice; //true je black, false je white
    protected int antRow, antColumn;
    protected AntOrientation antOrientation = AntOrientation.LEFT;
    protected AntState antState = AntState.ZERO;

    private long step = 0;
    private int skipPreviousQuantity = 1, skipNextQuantity = 1;
    private boolean playing = false;
    private Timer timer;

    private static int ANT_MOVING_PERIOD_IN_MS = 200;
    private final ArrayList<Integer> antSpeedItems = new ArrayList<>(Arrays.asList(1, 2, 5, 10, 20, 50));

    protected void initialize() {
        gridLayout = findViewById(R.id.lattice);
        refreshLattice();

        setChangeSpeedListener();
        setPlayPauseListener();
        setScreenshotAndVideoListeners();

        step = 0;
        generateGridLayout();
    }

    private void refreshLattice() {
        numberOfRows = 11;
        numberOfColumns = 11;
        antRow = 5;
        antColumn = 5;
        antOrientation = AntOrientation.LEFT;
        antState = AntState.ZERO;
        step = 0;

        lattice = new ArrayList<>(numberOfRows);
        for (int i = 0; i < numberOfRows; i++) {
            ArrayList<Boolean> newRow = new ArrayList<>(numberOfColumns);
            for (int j = 0; j < numberOfColumns; j++)
                newRow.add(false);
            lattice.add(newRow);
        }
    }

    private void setChangeSpeedListener() {
        Spinner antSpeedDropdown = findViewById(R.id.antSpeedDropdown);
        antSpeedDropdown.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, antSpeedItems));
        antSpeedDropdown.setSelection(antSpeedItems.indexOf(1000 / ANT_MOVING_PERIOD_IN_MS));
        antSpeedDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ANT_MOVING_PERIOD_IN_MS = 1000 / Integer.parseInt(parent.getItemAtPosition(position).toString());
                if (playing) {
                    timer.cancel();
                    timer = createTimer();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setPlayPauseListener() {
        findViewById(R.id.playPauseButton).setOnClickListener(v -> {
            Button skipPreviousQuantityButton = findViewById(R.id.skipPreviousQuantityButton);
            Button skipNextQuantityButton = findViewById(R.id.skipNextQuantityButton);
            Button numberOfStepsButton = findViewById(R.id.numberOfStepsButton);
            if (playing) {//pause
                timer.cancel();
                ((ImageView) v).setImageResource(R.drawable.outline_play_arrow_black_24dp);
                ((ImageView) findViewById(R.id.skipPreviousButton)).setImageResource(R.drawable.outline_skip_previous_black_24dp);
                ((ImageView) findViewById(R.id.skipNextButton)).setImageResource(R.drawable.outline_skip_next_black_24dp);
                skipPreviousQuantityButton.setBackgroundColor(getResources().getColor(R.color.lightBlue));
                skipPreviousQuantityButton.setTextColor(getResources().getColor(R.color.black));
                skipPreviousQuantityButton.setEnabled(true);
                skipNextQuantityButton.setBackgroundColor(getResources().getColor(R.color.lightBlue));
                skipNextQuantityButton.setTextColor(getResources().getColor(R.color.black));
                skipNextQuantityButton.setEnabled(true);
                numberOfStepsButton.setBackgroundColor(getResources().getColor(R.color.grayish));
                numberOfStepsButton.setEnabled(true);
            } else {//resume
                timer = createTimer();
                ((ImageView) v).setImageResource(R.drawable.outline_pause_black_24dp);
                ((ImageView) findViewById(R.id.skipPreviousButton)).setImageResource(R.drawable.outline_skip_previous_gray_24dp);
                ((ImageView) findViewById(R.id.skipNextButton)).setImageResource(R.drawable.outline_skip_next_gray_24dp);
                skipPreviousQuantityButton.setBackgroundColor(getResources().getColor(R.color.lighterBlue));
                skipPreviousQuantityButton.setTextColor(getResources().getColor(R.color.gray));
                skipPreviousQuantityButton.setEnabled(false);
                skipNextQuantityButton.setBackgroundColor(getResources().getColor(R.color.lighterBlue));
                skipNextQuantityButton.setTextColor(getResources().getColor(R.color.gray));
                skipNextQuantityButton.setEnabled(false);
                numberOfStepsButton.setBackgroundColor(getResources().getColor(R.color.lighterGrayish));
                numberOfStepsButton.setEnabled(false);
            }
            playing = !playing;
        });

        findViewById(R.id.skipPreviousButton).setOnClickListener(v -> {
            if (!playing) {
                long destination = step - skipPreviousQuantity;
                if (destination < 0)
                    destination = 0;
                goToDestinationStep(destination);
            }
        });

        findViewById(R.id.skipNextButton).setOnClickListener(v -> {
            if (!playing) {
                long destination = step + skipNextQuantity;
                goToDestinationStep(destination);
            }
        });
    }

    private boolean checkWriteExternalStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            int REQUEST_CODE_CONTACT = 101;
            String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

            if (this.checkSelfPermission(permissions[0]) != PackageManager.PERMISSION_GRANTED) {
                this.requestPermissions(permissions, REQUEST_CODE_CONTACT);
                return false;
            }
        }
        return true;
    }

    private void setScreenshotAndVideoListeners() {
        findViewById(R.id.screenshotButton).setOnClickListener(v -> {
            if (!checkWriteExternalStoragePermission())
                return;

            View rootView = gridLayout.getRootView();
            rootView.setDrawingCacheEnabled(true);
            rootView.buildDrawingCache(true);
            Bitmap bitmap = rootView.getDrawingCache();

            String name = getSupportActionBar().getTitle().toString().replace(' ', '_');
            //String stepNumber = "_step_number_" + step;
            String timestamp = new SimpleDateFormat("_yyyyMMdd_HHmm").format(new Date());
            File file = new File(Environment.getExternalStorageDirectory().toString() + "/" + name + timestamp + ".jpg");
            try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
                //file.createNewFile();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                fileOutputStream.flush();
                MediaStore.Images.Media.insertImage(getContentResolver(),
                        file.getAbsolutePath(), file.getName(), file.getName());
                Toast.makeText(this, "Screenshot created: " + file.getAbsolutePath(), Toast.LENGTH_LONG).show();
            } catch (FileNotFoundException e) {
                Log.e("ERROR", e.getMessage(), e);
            } catch (IOException e) {
                Log.e("ERROR", e.getMessage(), e);
            }

            rootView.setDrawingCacheEnabled(false);
        });

        findViewById(R.id.videoButton).setOnClickListener(v -> {
            if (!checkWriteExternalStoragePermission())
                return;

        });
    }

    private void goToDestinationStep(long destination) {
        if (destination < step)
            refreshLattice();

        for (; step < destination; step++) {
            moveAnt();
            if (checkIfLatticeIsTooBig()) {
                break;
            }
        }

        updateNumberOfSteps();
        generateGridLayout();
    }

    private Timer createTimer() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> {
                    moveAnt();
                    step++;
                    updateNumberOfSteps();
                    if (checkIfLatticeIsTooBig()) {
                        this.cancel();
                    }
                });
            }
        }, 0, ANT_MOVING_PERIOD_IN_MS);
        return timer;
    }

    private boolean checkIfLatticeIsTooBig() {
        return (numberOfRows * numberOfColumns) > 17000;
    }

    private void autoZoomOut() {
        ((ZoomLayout) findViewById(R.id.zoomLayout)).getEngine().setContentSize(102 * numberOfColumns, 102 * numberOfRows, true);//toDo
    }

    private void updateNumberOfSteps() {
        ((Button) findViewById(R.id.numberOfStepsButton)).setText(String.valueOf(step));
    }

    protected boolean checkIfCurrentlyPlaying() {
        return playing;
    }

    private void generateGridLayout() {
        gridLayout.removeAllViews();//

        gridLayout.setRowCount(numberOfRows);
        gridLayout.setColumnCount(numberOfColumns);

        for (int i = 0; i < numberOfRows; i++) {
            for (int j = 0; j < numberOfColumns; j++) {
                gridLayout.addView(generateImageView(lattice.get(i).get(j)));
            }
        }

        ImageView antSquare = getCurrentPositionImageView();
        switch (antOrientation) {
            case UP:
                antSquare.setImageResource(R.drawable.ant_up);
                break;
            case RIGHT:
                antSquare.setImageResource(R.drawable.ant_right);
                break;
            case DOWN:
                antSquare.setImageResource(R.drawable.ant_down);
                break;
            case LEFT:
                antSquare.setImageResource(R.drawable.ant_left);
                break;
        }

        autoZoomOut();
    }

    private ImageView generateImageView(boolean color) {//
        ImageView imageView = new ImageView(this);
        setColor(imageView, color);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(100, 100));
        return imageView;
    }

    protected abstract void moveAnt();

    protected AntOrientation turnRight() { //turnClockwise
        switch (antOrientation) {
            case UP:
                return AntOrientation.RIGHT;
            case RIGHT:
                return AntOrientation.DOWN;
            case LEFT:
                return AntOrientation.UP;
            default: //DOWN
                return AntOrientation.LEFT;
        }
    }

    protected AntOrientation turnLeft() { //turnCounterClockwise
        switch (antOrientation) {
            case RIGHT:
                return AntOrientation.UP;
            case DOWN:
                return AntOrientation.RIGHT;
            case LEFT:
                return AntOrientation.DOWN;
            default: //UP
                return AntOrientation.LEFT;
        }
    }

    protected AntOrientation Uturn() {
        switch (antOrientation) {
            case UP:
                return AntOrientation.DOWN;
            case DOWN:
                return AntOrientation.UP;
            case LEFT:
                return AntOrientation.RIGHT;
            default: //RIGHT
                return AntOrientation.LEFT;
        }
    }

    protected void moveForwardOneUnit() {
        switch (antOrientation) {
            case UP:
                antRow--;
                if (antRow < 0) {
                    addLatticeRowToStart();
                    antRow++;
                }
                if (checkIfCurrentlyPlaying()) {
                    getCurrentPositionImageView().setImageResource(R.drawable.ant_up);
                }
                break;
            case RIGHT:
                antColumn++;
                if (antColumn >= numberOfColumns) {
                    addLatticeColumnToEnd();
                }
                if (checkIfCurrentlyPlaying()) {
                    getCurrentPositionImageView().setImageResource(R.drawable.ant_right);
                }
                break;
            case DOWN:
                antRow++;
                if (antRow >= numberOfRows) {
                    addLatticeRowToEnd();
                }
                if (checkIfCurrentlyPlaying()) {
                    getCurrentPositionImageView().setImageResource(R.drawable.ant_down);
                }
                break;
            case LEFT:
                antColumn--;
                if (antColumn < 0) {
                    addLatticeColumnToStart();
                    antColumn++;
                }
                if (checkIfCurrentlyPlaying()) {
                    getCurrentPositionImageView().setImageResource(R.drawable.ant_left);
                }
                break;
        }
    }

    protected ImageView getCurrentPositionImageView() {
        return (ImageView) gridLayout.getChildAt(antRow * numberOfColumns + antColumn);
    }

    private void setColor(ImageView imageView, boolean color) {
        imageView.setBackgroundColor(color ? COLOR_BLACK : COLOR_WHITE);
        if (!color) { //white
            imageView.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.image_border, null));
        }
    }

    protected void changeColor(ImageView imageView, boolean color) {
        setColor(imageView, color);
        imageView.setImageResource(android.R.color.transparent);
    }

    private void addLatticeRowToStart() {
        ArrayList<Boolean> newRow = new ArrayList<>(numberOfColumns);
        for (int j = 0; j < numberOfColumns; j++)
            newRow.add(false);
        lattice.add(0, newRow);
        numberOfRows++;

        if (checkIfCurrentlyPlaying()) {
            gridLayout.setRowCount(numberOfRows);
            for (int i = 0; i < numberOfColumns; i++) {
                gridLayout.addView(generateImageView(false), 0);
            }
            autoZoomOut();
        }
    }

    private void addLatticeRowToEnd() {
        ArrayList<Boolean> newRow = new ArrayList<>(numberOfColumns);
        for (int j = 0; j < numberOfColumns; j++)
            newRow.add(false);
        lattice.add(newRow);
        numberOfRows++;

        if (checkIfCurrentlyPlaying()) {
            gridLayout.setRowCount(numberOfRows);
            for (int i = 0; i < numberOfColumns; i++) {
                gridLayout.addView(generateImageView(false));
            }
            autoZoomOut();
        }
    }

    private void addLatticeColumnToStart() {
        for (ArrayList<Boolean> row : lattice)
            row.add(0, false);
        numberOfColumns++;

        if (checkIfCurrentlyPlaying()) {
            gridLayout.setColumnCount(numberOfColumns);
            for (int i = 0; i < numberOfRows; i++) {
                gridLayout.addView(generateImageView(false), i * numberOfColumns);
            }
            autoZoomOut();
        }
    }

    private void addLatticeColumnToEnd() {
        for (ArrayList<Boolean> row : lattice)
            row.add(false);
        numberOfColumns++;

        if (checkIfCurrentlyPlaying()) {
            gridLayout.setColumnCount(numberOfColumns);
            for (int i = 0; i < numberOfRows; i++) {
                gridLayout.addView(generateImageView(false), i * numberOfColumns + numberOfColumns - 1);
            }
            autoZoomOut();
        }
    }

    public void pickSkipPreviousQuantity(View view) {
        final LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.number_picker_layout, null);
        NumberPicker numberPicker = linearLayout.findViewById(R.id.numberPicker);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(999);
        numberPicker.setValue(skipPreviousQuantity);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Pick number of steps for going back");
        alertDialog.setView(linearLayout);
        alertDialog.setCancelable(false);

        alertDialog.setNegativeButton("Cancel", (dialog, which) -> {
            dialog.cancel();
        }).setPositiveButton("Confirm", (dialog, which) -> {
            skipPreviousQuantity = numberPicker.getValue();
            ((Button) findViewById(R.id.skipPreviousQuantityButton)).setText(String.valueOf(skipPreviousQuantity));
            dialog.cancel();
        });
        alertDialog.create();
        alertDialog.show();
    }

    public void pickSkipNextQuantity(View view) {
        final LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.number_picker_layout, null);
        NumberPicker numberPicker = linearLayout.findViewById(R.id.numberPicker);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(999);
        numberPicker.setValue(skipNextQuantity);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Pick number of steps for going forward");
        alertDialog.setView(linearLayout);
        alertDialog.setCancelable(false);

        alertDialog.setNegativeButton("Cancel", (dialog, which) -> {
            dialog.cancel();
        }).setPositiveButton("Confirm", (dialog, which) -> {
            skipNextQuantity = numberPicker.getValue();
            ((Button) findViewById(R.id.skipNextQuantityButton)).setText(String.valueOf(skipNextQuantity));
            dialog.cancel();
        });
        alertDialog.create();
        alertDialog.show();
    }

    public void pickNumberOfSteps(View view) {
        final LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.number_field_layout, null);
        EditText numberField = linearLayout.findViewById(R.id.numberOfSteps);
        numberField.setText(String.valueOf(step));
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Enter step number on which you want to go");
        alertDialog.setView(linearLayout);
        alertDialog.setCancelable(false);

        alertDialog.setNegativeButton("Cancel", (dialog, which) -> {
            dialog.cancel();
        }).setPositiveButton("Confirm", (dialog, which) -> {
            long numberOfStepsInField = Long.parseLong(numberField.getText().toString());
            if (numberOfStepsInField != step)
                goToDestinationStep(numberOfStepsInField);
            dialog.cancel();
        });
        alertDialog.create();
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.extended_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        } else if (id == R.id.menuHelp) {
            Intent intent = new Intent(this, HelpActivity.class);
            startActivity(intent);
        }
        return true;
    }

    enum AntOrientation {UP, RIGHT, DOWN, LEFT}

    enum AntState {ZERO, ONE}

    enum AntTurn {L, R, U, N}

}
