package com.example.langtonsant;

import android.os.Bundle;
import android.widget.ImageView;

public class LangtonsAntActivity extends AntActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ant);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Langton's Ant simulation");

        initialize();
    }

    protected void moveAnt() {
        Boolean prevColor = lattice.get(antRow).get(antColumn);
        if (prevColor) { // black
            antOrientation = turnLeft();
        } else { // white
            antOrientation = turnRight();
        }
        flipColorOfTheSquare(prevColor);
        moveForwardOneUnit();
    }

    protected void flipColorOfTheSquare(Boolean prevColor) {
        lattice.get(antRow).set(antColumn, !prevColor);

        if (checkIfCurrentlyPlaying()) {
            ImageView prevPositionImage = getCurrentPositionImageView();
            changeColor(prevPositionImage, !prevColor);
        }
    }

}