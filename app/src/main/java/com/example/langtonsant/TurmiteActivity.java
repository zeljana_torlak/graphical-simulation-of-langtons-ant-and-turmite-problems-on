package com.example.langtonsant;

import android.os.Bundle;
import android.widget.ImageView;

public class TurmiteActivity extends AntActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ant);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Turmite simulation");

        initialize();
    }

    protected void moveAnt() {
        Boolean prevColor = lattice.get(antRow).get(antColumn);
        int turnOrdinal = StateTransitionTableActivity.TURMITE_STATE_TRANSITION_TABLE[antState.ordinal()][prevColor ? 4 : 1];
        switch (AntTurn.values()[turnOrdinal]) {
            case L:
                antOrientation = turnLeft();
                break;
            case R:
                antOrientation = turnRight();
                break;
            case U:
                antOrientation = Uturn();
                break;
        }
        flipColorOfTheSquare(prevColor);
        moveForwardOneUnit();

        updateAntState(prevColor);
    }

    protected void flipColorOfTheSquare(Boolean prevColor) {
        int newColorCode = StateTransitionTableActivity.TURMITE_STATE_TRANSITION_TABLE[antState.ordinal()][prevColor ? 3 : 0];

        boolean newColor = newColorCode == COLOR_BLACK;
        lattice.get(antRow).set(antColumn, newColor);

        if (checkIfCurrentlyPlaying()) {
            ImageView prevPositionImage = getCurrentPositionImageView();
            changeColor(prevPositionImage, newColor);
        }
    }

    private void updateAntState(Boolean prevColor) {
        int newStateOrdinal = StateTransitionTableActivity.TURMITE_STATE_TRANSITION_TABLE[antState.ordinal()][prevColor ? 5 : 2];
        antState = AntState.values()[newStateOrdinal];
    }

}